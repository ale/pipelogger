
#define _GNU_SOURCE
#include "config.h"
#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <syslog.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <string.h>
#include <stdlib.h>

static int usage() {
  fprintf(stderr,
          "%s - exec a program with its output sent to syslog\n"
          "Usage: %s [<OPTIONS>] <COMMAND> [<ARGS>...]\n"
          "\n"
          "Options:\n"
          "\n"
          " -p, --priority PRIO   mark given message with this priority (in the\n"
          "                       format facility.level, default is user.notice)\n"
          " -t, --tag <tag>       mark every line with this tag (the default is\n"
          "                       COMMAND)\n"
          "\n", PACKAGE, PACKAGE);
  return 2;
}

struct facility_name {
  const char *name;
  int facility;
} facility_names[] = {
  {"auth", LOG_AUTH},
  {"cron", LOG_CRON},
  {"daemon", LOG_DAEMON},
  {"ftp", LOG_FTP},
  {"kern", LOG_KERN},
  {"lpr", LOG_LPR},
  {"uucp", LOG_UUCP},
  {"mail", LOG_MAIL},
  {"user", LOG_USER},
  {"local0", LOG_LOCAL0},
  {"local1", LOG_LOCAL1},
  {"local2", LOG_LOCAL2},
  {"local3", LOG_LOCAL3},
  {"local4", LOG_LOCAL4},
  {"local5", LOG_LOCAL5},
  {"local6", LOG_LOCAL6},
  {"local7", LOG_LOCAL7},
  {NULL, 0}
};

struct level_name {
  const char *name;
  int level;
} level_names[] = {
  {"emerg", LOG_EMERG},
  {"alert", LOG_ALERT},
  {"crit", LOG_CRIT},
  {"err", LOG_ERR},
  {"error", LOG_ERR},
  {"warn", LOG_WARNING},
  {"warning", LOG_WARNING},
  {"notice", LOG_NOTICE},
  {"info", LOG_INFO},
  {"debug", LOG_DEBUG},
  {NULL, 0}
};

static int parse_facility(char *f, int *facility) {
  struct facility_name *nptr;
  for (nptr = facility_names; nptr->name; nptr++) {
    if (!strcasecmp(f, nptr->name)) {
      *facility = nptr->facility;
      return 0;
    }
  }
  return -1;
}

static int parse_level(char *l, int *level) {
  struct level_name *nptr;
  for (nptr = level_names; nptr->name; nptr++) {
    if (!strcasecmp(l, nptr->name)) {
      *level = nptr->level;
      return 0;
    }
  }
  return -1;
}

static int parse_priority(const char *p, int *facility, int *level) {
  char *ll, *lp;
  ll = strdup(p);
  if ((lp = strchr(ll, '.')) == NULL) {
    return -1;
  }
  *lp++ = '\0';
  if (0 != parse_facility(ll, facility)) {
    return -1;
  }
  if (0 != parse_level(lp, level)) {
    return -1;
  }
  return 0;
}

static int pid = -1;
static int status = 0;

// Returns an exit status code.
int fork_and_exec(int level, char **argv) {
  int pipefd[2];

  if (0 != pipe(pipefd)) {
    fprintf(stderr, "Could not create pipe: %s\n", strerror(errno));
    return 1;
  }

  pid = fork();
  if (pid == 0) {
    // Child.
    close(0);
    close(1);
    close(2);
    dup2(pipefd[1], 1);
    dup2(pipefd[1], 2);
    close(pipefd[0]);
    close(pipefd[1]);
    if (0 != execvp(argv[0], argv)) {
      fprintf(stderr, "Could not exec %s: %s\n", argv[0], strerror(errno));
    }
    exit(1);
  } else {
    FILE *fp = fdopen(pipefd[0], "r");
    char buf[4096], *line;
    
    close(pipefd[1]);

    while ((line = fgets(buf, sizeof(buf), fp)) != NULL) {
      syslog(level, "%s", line);
    }

    fclose(fp);

    // Wait for the child process to exit.
    if (WIFEXITED(status)) {
      return WEXITSTATUS(status);
    } else {
      return 1;
    }
  }
}

void child_reaper(int sig) {
  pid_t child_pid;

  while ((child_pid = waitpid(-1, &status, WNOHANG)) > 0) {
    ;
  }
}

void signal_passthru(int sig) {
  if (pid > 0) {
    kill(pid, sig);
  }
}

int main(int argc, char **argv) {
  int c;
  char *tag = NULL;
  int level = LOG_NOTICE;
  int facility = LOG_USER;
  static int log_pid = 0;
  int log_opts;

  while (1) {
    int option_index = 0;
    const char *opt_name = NULL;
    static struct option long_options[] = {
      {"help", no_argument, 0, 'h'},
      {"tag", required_argument, 0, 't'},
      {"priority", required_argument, 0, 'p'},
      {"pid", no_argument, &log_pid, 1},
      {0, 0, 0, 0},
    };

    c = getopt_long(argc, argv, "h", long_options, &option_index);
    if (c < 0) {
      break;
    }

    switch (c) {
    case 0:
      opt_name = long_options[option_index].name;
      if (!strcmp(opt_name, "help")) {
        usage();
        exit(0);
      }
      break;

    case 't':
      tag = optarg;
      break;

    case 'p':
      if (0 != parse_priority(optarg, &facility, &level)) {
        fprintf(stderr, "Could not parse priority '%s'\n", optarg);
        exit(1);
      }
      break;

    case 'h':
      usage();
      exit(0);

    case '?':
      exit(usage());
    }
  }

  if (optind >= argc) {
    fprintf(stderr, "Not enough arguments\n\n");
    exit(usage());
  }

  if (tag == NULL) {
    tag = argv[optind];
  }

  log_opts = LOG_NDELAY;
  if (log_pid) {
    log_opts |= LOG_PID;
  }
  openlog(tag, log_opts, facility);

  signal(SIGCHLD, child_reaper);

  // Pass thru a bunch of signals.
  signal(SIGTERM, signal_passthru);
  signal(SIGINT, signal_passthru);
  signal(SIGHUP, signal_passthru);
  signal(SIGUSR1, signal_passthru);
  signal(SIGUSR2, signal_passthru);
  signal(SIGSTOP, signal_passthru);
  signal(SIGTSTP, signal_passthru);
  signal(SIGCONT, signal_passthru);

  return fork_and_exec(level, argv + optind);
}
